from quiz import Quiz
import random
import numpy as np
from itertools import permutations
import sys
sys.setrecursionlimit(100000000)


class DeterministicQuiz(Quiz):
    def __init__(self, N, highReward=10, lowReward=0, minProbabiliy=0, maxProbabiliy=1, M=None, density=None, allowPass=True):
        super().__init__(N, highReward=highReward, lowReward=lowReward, minProbabiliy=minProbabiliy, maxProbabiliy=maxProbabiliy, allowPass=allowPass)
        self.M = N if M is None else min(M, N)
        self.density = 1 if density is None else min(density, 1)
        self.generateConstraints(density)
        self.start = 0

    def defineBaseHeuristic(self, heuristic):
        if heuristic == 'index':
            self.baseHeuristic = self.indexPolicy
        elif heuristic == 'greedy':
            self.baseHeuristic = self.greedyPolicy
        elif heuristic == 'random':
            self.baseHeuristic = self.randomPolicy
        else:
            raise Exception('The heuristic is not supported')

    def indexPolicy(self, questions=None):
        return super().indexPolicy(questions)[:self.M]

    def indexPolicyWithWindow(self, questions=None):
        questions = self.questions if questions is None else questions
        window, order, orderSet, score = self.window, [], set(), self.scoreIndex
        for i in range(self.start, self.M):
            s = window[i] - orderSet
            s.add(self.N)
            q = -1
            v_max = - float('inf')
            for option in s:
                v = score(option)
                if v > v_max:
                    v_max, q = v, option
            order.append(q)
            orderSet.add(q)
        return order

    def greedyPolicy(self, questions=None):
        return super().greedyPolicy(questions)[:self.M]

    def greedyPolicyWithWindow(self, questions=None):
        questions = self.questions if questions is None else questions
        window, order, orderSet, score = self.window, [], set(), self.scoreGreedy
        for i in range(self.start, self.M):
            s = window[i] - orderSet
            s.add(self.N)
            q = -1
            v_max = - float('inf')
            for option in s:
                v = score(option)
                if v > v_max:
                    v_max, q = v, option
            order.append(q)
            orderSet.add(q)
        return order

    def randomPolicy(self, questions=None):
        return super().randomPolicy(questions)[:self.M]

    def randomPolicyWithWindow(self, questions=None):
        questions = self.questions if questions is None else questions
        window, order, orderSet = self.window, [], set()
        for i in range(self.start, self.M):
            s = window[i] - orderSet
            s.add(self.N)
            if len(s) == 0:
                continue
            q = random.choice(list(s))
            order.append(q)
            orderSet.add(q)
        return order

    def optimalPolicy(self, questions=None):
        questions = self.questions if questions is None else questions
        v_max = -1
        V = self.V
        for i in permutations(questions + [self.N for _ in range(self.M)], self.M):
            v = V(list(i))
            if v > v_max:
                v_max, schedule_max = v, i
        return schedule_max

    def optimalPolicyWithWindow(self, questions=None):
        questions = self.questions if questions is None else questions
        start, window, order, orderSet = self.N - len(questions) + 1, self.window, [], set()
        v_max = -1
        V = self.V
        for i in self.possible():
            v = V(list(i))
            if v > v_max:
                v_max, schedule_max = v, i
        return schedule_max

    def possible(self, questions=set(), start=0):
        options = self.window[start] - questions
        options.add(self.N)
        if start == self.M - 1:
            for option in options:
                yield [option]
        else:
            for option in options:
                for result in self.possible(questions.union(set([option])), start=start+1):
                    yield [option] + result

    def H(self, P):
        """Heuristic Reward"""
        P_set = set(P)
        self.start = len(P)
        P_comp = [i for i in self.questions if i not in P_set]
        comp_schedule = self.baseHeuristic(P_comp)
        schedule = P+comp_schedule
        return self.V(schedule[:self.M])

    def heuristicRollout(self, heuristic='index', m=1, n=None):
        self.defineBaseHeuristic(heuristic)
        schedule = []
        questions = set(self.questions)
        H = self.H

        for q in range(self.M):
            v_max = -1
            i_max = self.N
            if n is None or m == 1:
                for i in permutations(questions, m):
                    i = list(i)
                    v = H(schedule+i)
                    if v > v_max:
                        v_max, i_max = v, i[0]
            else:
                I_new = {tuple([]): -1}
                for _ in range(m):
                    I_old = sorted(I_new, key=I_new.get, reverse=True)[:n]
                    I_new = {}
                    questions.add(self.N)
                    for i in I_old:
                        for j in questions:
                            if j in i and j != self.N:
                                continue
                            I = list(i)+[j]
                            P = schedule+I
                            I_new[tuple(I)] = H(P)
                        if len(I_new) == 0:
                            I_new[i] = 1
                i_max = sorted(I_new, key=I_new.get, reverse=True)[0][0]
            schedule.append(i_max)
            questions.discard(i_max)

        return schedule[:self.M]

    def generateConstraints(self, density):
        if self.density < 1:
            constrains = {}
            for answer in range(self.M):
                constrains[answer] = set([len(self.questions)-1])
                for question in self.questions:
                    if random.random() < density:
                        constrains[answer].add(question)
            self.window = constrains
            self.constraints = self.constraintsWithWindow
            self.randomPolicy = self.randomPolicyWithWindow
            self.greedyPolicy = self.greedyPolicyWithWindow
            self.indexPolicy = self.indexPolicyWithWindow
            self.optimalPolicy = self.optimalPolicyWithWindow

    def constraintsWithWindow(self, order):
        for i, q in enumerate(order):
            if q not in self.window[i]:
                return False
        return True

    def constraints(self, order):
        return True
