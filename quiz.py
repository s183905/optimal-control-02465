from functools import partial
import numpy as np
from functools import reduce
import random
from copy import deepcopy
from itertools import permutations
import operator
import time


class Quiz():
    def __init__(self, N, highReward=10, lowReward=1, minProbabiliy=0, maxProbabiliy=1, allowPass=False):
        self.N = N
        self.questions = list(range(N))
        self.probabilities = np.random.uniform(low=minProbabiliy, high=maxProbabiliy, size=N)
        self.rewards = np.random.uniform(low=lowReward, high=highReward, size=N)
        if allowPass:
            self.questions.append(len(self.questions))
            self.probabilities = np.append(self.probabilities, [1])
            self.rewards = np.append(self.rewards, [0])

    def indexPolicy(self, questions=None):
        questions = self.questions if questions is None else questions
        return sorted(questions, key=lambda i: self.scoreIndex(i), reverse=True)[:self.M]

    def greedyPolicy(self, questions=None):
        questions = self.questions if questions is None else questions
        return sorted(questions, key=lambda i: self.scoreGreedy(i), reverse=True)[:self.M]

    def randomPolicy(self, questions=None):
        questions = self.questions if questions is None else questions
        questions = deepcopy(questions)
        random.shuffle(questions)
        return questions[:self.M]

    def scoreIndex(self, i):
        p_i = self.probabilities[i]
        v_i = self.rewards[i]
        if p_i == 1:
            return v_i
        return p_i*v_i / (1 - p_i)

    def scoreGreedy(self, i):
        p_i = self.probabilities[i]
        v_i = self.rewards[i]
        return p_i*v_i

    def answerQuestion(self, question):
        return None if np.random.uniform() > self.probabilities[question] else self.rewards[question]

    def V(self, order):
        """Expected Reward"""
        if not self.constraints(order):
            return -float('inf')
        probabilities = self.probabilities
        rewards = self.rewards
        self.start = 0
        return reduce(lambda c, new: probabilities[new] * (rewards[new] + c), reversed(order), 0)

    def MontoCarlo(self, policy, n=1000):
        total = 0
        for _ in range(n):
            order = policy()
            for question in order:
                reward = self.answerQuestion(question)
                if reward is None:
                    break
                total += reward
        return total / n

    def constraints(self, order):
        return True
