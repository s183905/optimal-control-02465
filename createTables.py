from table1 import table1
from table2 import table2
from table3 import table3

print('Table 1 - Compared to optimal (N=20, M=12)')
table1(20, M=12)

print('\n\nTable 2 - Compared to optimal (N=20, M=7)')
table2(20, M=7)

print('\n\nTable 3 - Timing')
table3()
