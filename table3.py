from deterministic import DeterministicQuiz
from table import line, dataRows
import numpy as np
import random
import time
import sys

SEED = 1


def table3(minProbabiliy=0.2, density=0.1):
    start = time.time()
    random.seed(SEED)
    np.random.seed(SEED)
    probs, n = [12, 14, 17, 25, 50, 75], 30
    optimal = [0, 0, 0, '---', '---', '---']
    greedy_heuristic, one_step_greedy, two_step_greedy, index_heuristic, one_step_index, two_step_index = ([0 for _ in probs] for _ in range(6))
    for p, prob in enumerate(probs):
        for i in range(n):
            sys.stdout.write('#')
            sys.stdout.flush()
            quiz = DeterministicQuiz(prob, minProbabiliy=minProbabiliy, density=density)
            temp = time.time()
            if p < 3:
                quiz.V(quiz.optimalPolicy())
                optimal[p] += time.time() - temp

            temp = time.time()
            quiz.V(quiz.greedyPolicy())
            greedy_heuristic[p] += time.time() - temp

            temp = time.time()
            quiz.V(quiz.heuristicRollout(heuristic='greedy', m=1))
            one_step_greedy[p] += time.time() - temp

            temp = time.time()
            quiz.V(quiz.heuristicRollout(heuristic='greedy', m=2, n=4))
            two_step_greedy[p] += time.time() - temp

            temp = time.time()
            quiz.V(quiz.indexPolicy())
            index_heuristic[p] += time.time() - temp

            temp = time.time()
            quiz.V(quiz.heuristicRollout(heuristic='index', m=1))
            one_step_index[p] += time.time() - temp

            temp = time.time()
            quiz.V(quiz.heuristicRollout(heuristic='index', m=2, n=4))
            two_step_index[p] += time.time() - temp

        if p < 3:
            optimal[p] = int(optimal[p]/n*10) / 10
        greedy_heuristic[p] = int(greedy_heuristic[p]/n*10) / 10
        one_step_greedy[p] = int(one_step_greedy[p]/n*10) / 10
        two_step_greedy[p] = int(two_step_greedy[p]/n*10) / 10
        index_heuristic[p] = int(index_heuristic[p]/n*10) / 10
        one_step_index[p] = int(one_step_index[p]/n*10) / 10
        two_step_index[p] = int(two_step_index[p]/n*10) / 10
        print(f'| {prob}')

    header = f' N Questions        {"        ".join([str(prob) for prob in probs])} '
    l = len(header)

    print('')
    line(l)
    print(header)
    line(l)
    dataRows([('Optimal Policy  ', optimal)])
    line(l)
    dataRows([('Greedy Heuristic', greedy_heuristic), ('One-Step Rollout', one_step_greedy), ('Two-Step Rollout', two_step_greedy)])
    line(l)
    dataRows([('Index Heuristic ', index_heuristic), ('One-Step Rollout', one_step_index), ('Two-Step Rollout', two_step_index)])
    line(l)
    print('')
    print(f'Calculated in {time.time() - start:.6} seconds')


if __name__ == "__main__":
    table3()
