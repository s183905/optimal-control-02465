def line(l):
    print(''.join(['_' for i in range(l+1)]))


def dataRows(data, optimalEnabled=False):
    percent = '%' if optimalEnabled else ' '
    for name, values in data:
        print(f'{name}   {"      ".join([str(v)+percent for v in values])}')
