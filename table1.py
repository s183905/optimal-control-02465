from deterministic import DeterministicQuiz
from table import line, dataRows
import numpy as np
import random
import time
import sys

SEED = 1


def table1(N, M=None, optimalEnabled=True):
    start = time.time()
    random.seed(SEED)
    np.random.seed(SEED)
    probs, n = [0.2, 0.4, 0.6, 0.8], 30
    greedy_heuristic, one_step_greedy, two_step_greedy, index_heuristic, one_step_index, two_step_index = ([0 for _ in probs] for _ in range(6))
    for p, prob in enumerate(probs):
        for i in range(n):
            sys.stdout.write('#')
            sys.stdout.flush()
            quiz = DeterministicQuiz(N, M=M, minProbabiliy=prob, density=0.1)
            optimal = quiz.V(quiz.optimalPolicy()) if optimalEnabled else 100

            greedy_heuristic[p] += quiz.V(quiz.greedyPolicy()) / optimal
            one_step_greedy[p] += quiz.V(quiz.heuristicRollout(heuristic='greedy', m=1)) / optimal
            two_step_greedy[p] += quiz.V(quiz.heuristicRollout(heuristic='greedy', m=2, n=4)) / optimal

            index_heuristic[p] += quiz.V(quiz.indexPolicy()) / optimal
            one_step_index[p] += quiz.V(quiz.heuristicRollout(heuristic='index', m=1)) / optimal
            two_step_index[p] += quiz.V(quiz.heuristicRollout(heuristic='index', m=2, n=4)) / optimal

        greedy_heuristic[p] = int(greedy_heuristic[p]/n*1000) / 10
        one_step_greedy[p] = int(one_step_greedy[p]/n*1000) / 10
        two_step_greedy[p] = int(two_step_greedy[p]/n*1000) / 10
        index_heuristic[p] = int(index_heuristic[p]/n*1000) / 10
        one_step_index[p] = int(one_step_index[p]/n*1000) / 10
        two_step_index[p] = int(two_step_index[p]/n*1000) / 10
        print(f'| {prob}')

    header = f' Minimum Prob.      {"        ".join([str(prob) for prob in probs])} '
    l = len(header)

    print('')
    line(l)
    print(header)
    print('  of Success')
    line(l)
    dataRows([('Greedy Heuristic', greedy_heuristic), ('One-Step Rollout', one_step_greedy), ('Two-Step Rollout', two_step_greedy)], optimalEnabled)
    line(l)
    dataRows([('Index Heuristic ', index_heuristic), ('One-Step Rollout', one_step_index), ('Two-Step Rollout', two_step_index)], optimalEnabled)
    line(l)
    print('')
    print(f'Calculated in {time.time() - start:.6} seconds')


if __name__ == "__main__":
    table1(20, M=12)
