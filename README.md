# Overview

All code has been witten for this project, and has not been copied or refactored from other sources.

The project consists of a simple implementation of the quiz problem in its simple form in the file `quiz.py`.
The determitic environment then introduces modifications to the simple quiz problem in the file `deterministic.py`.

Furtermore, `table.py` introduces some helper functions for the 3 tables found in `table1.py`, `table2.py` and `table3.py`.

All this is then collected in the `createTables.py`, that generate all the tables found in the project.
